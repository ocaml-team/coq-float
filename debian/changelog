coq-float (1:8.10.0-1) unstable; urgency=medium

  * Team upload
  * New upstream release, compiles with coq 8.11.0  (closes: #953765)
  * Add debian/upstream, containing the reference of the TPHOL paper

 -- Ralf Treinen <treinen@debian.org>  Mon, 16 Mar 2020 11:08:42 +0100

coq-float (1:8.9.0-1) unstable; urgency=medium

  * New upstream release (Closes: #813596)
  * Update Homepage and debian/watch
  * Update Vcs-*
  * Remove Samuel from Uploaders
  * Bump debhelper compat level to 12
  * Bump Standards-Version to 4.5.0
  * Add Rules-Requires-Root: no

 -- Stéphane Glondu <glondu@debian.org>  Sat, 01 Feb 2020 11:28:40 +0100

coq-float (1:8.4-5) unstable; urgency=medium

  * Recompile with OCaml 4.02.3

 -- Stéphane Glondu <glondu@debian.org>  Wed, 14 Oct 2015 11:37:12 +0200

coq-float (1:8.4-4) unstable; urgency=medium

  * Recompile with coq 8.4pl4

 -- Stéphane Glondu <glondu@debian.org>  Wed, 30 Jul 2014 08:59:29 +0200

coq-float (1:8.4-3) unstable; urgency=medium

  * Recompile with coq 8.4pl3
  * Bump Standards-Version to 3.9.5 (no changes)
  * Update Vcs-*

 -- Stéphane Glondu <glondu@debian.org>  Mon, 20 Jan 2014 07:44:07 +0100

coq-float (1:8.4-2) unstable; urgency=low

  * Recompile with OCaml 4.01.0

 -- Stéphane Glondu <glondu@debian.org>  Sat, 07 Dec 2013 10:06:37 +0100

coq-float (1:8.4-1) unstable; urgency=low

  * New upstream release
  * Use format version 1.0 in debian/copyright
  * Bump Standards-Version to 3.9.4
  * Bump debhelper compat level to 9

 -- Stéphane Glondu <glondu@debian.org>  Wed, 08 May 2013 23:29:32 +0200

coq-float (1:8.3pl1-4) unstable; urgency=low

  * Recompile with camlp5 6.06 (no changes)

 -- Stéphane Glondu <glondu@debian.org>  Wed, 06 Jun 2012 22:59:40 +0200

coq-float (1:8.3pl1-3) unstable; urgency=low

  * Recompile with coq 8.3pl4 and camlp5 6.05 (no changes)

 -- Stéphane Glondu <glondu@debian.org>  Fri, 30 Mar 2012 08:06:29 +0200

coq-float (1:8.3pl1-2) unstable; urgency=low

  * Recompile with camlp5 6.04 (no changes)

 -- Stéphane Glondu <glondu@debian.org>  Tue, 06 Mar 2012 08:30:25 +0100

coq-float (1:8.3pl1-1) unstable; urgency=low

  * New upstream release
    - remove all patches (applied upstream)

 -- Stéphane Glondu <glondu@debian.org>  Mon, 09 Jan 2012 20:41:12 +0100

coq-float (1:8.2-1.2-9) unstable; urgency=low

  * Rebuild with Coq 8.3pl3 (no source changes)

 -- Stéphane Glondu <glondu@debian.org>  Sun, 25 Dec 2011 17:51:31 +0100

coq-float (1:8.2-1.2-8) unstable; urgency=low

  * Recompile with OCaml 3.12.1 (no changes)
  * Bump Standards-Version to 3.9.2 (no changes)

 -- Stéphane Glondu <glondu@debian.org>  Thu, 03 Nov 2011 06:25:46 +0100

coq-float (1:8.2-1.2-7) unstable; urgency=low

  * Add patch to fix build with Coq 8.3
  * Bump Standards-Version to 3.9.1 (no changes)

 -- Stéphane Glondu <glondu@debian.org>  Tue, 19 Apr 2011 22:30:18 +0200

coq-float (1:8.2-1.2-6) unstable; urgency=low

  * Rebuild with coq 8.2.pl2+dfsg-2 (no changes)

 -- Stéphane Glondu <glondu@debian.org>  Thu, 10 Mar 2011 22:17:35 +0100

coq-float (1:8.2-1.2-5) unstable; urgency=low

  * Rebuild with Coq 8.2pl2
  * Update debian/watch
  * Bump Standards-Version to 3.9.0 (no changes)
  * Switch source package format to 3.0 (quilt)

 -- Stéphane Glondu <glondu@debian.org>  Sat, 03 Jul 2010 15:51:20 +0200

coq-float (1:8.2-1.2-4) unstable; urgency=low

  * Rebuild with OCaml 3.11.2
  * Rewrite debian/rules with dh overrides
  * Install *.vo files in user-contrib/Float
  * debian/control:
    - move to section math
    - bump dependency to debhelper
    - update my e-mail address, remove DMUA
    - update Standards-Version to 3.8.4 (no changes)

 -- Stéphane Glondu <glondu@debian.org>  Wed, 10 Feb 2010 21:47:18 +0100

coq-float (1:8.2-1.2-3) unstable; urgency=low

  * Rebuild with Coq 8.2pl1

 -- Stephane Glondu <steph@glondu.net>  Sat, 04 Jul 2009 14:06:56 +0200

coq-float (1:8.2-1.2-2) unstable; urgency=low

  * Recompile with OCaml 3.11.1 ABI
  * Update Standards-Version to 3.8.2

 -- Stephane Glondu <steph@glondu.net>  Wed, 01 Jul 2009 20:11:09 +0200

coq-float (1:8.2-1.2-1) unstable; urgency=low

  [ Samuel Mimram ]
  * Switch packaging to git.
  * Enforce strict dependency on coq ABI.
  * Update standards version to 3.8.0.
  * Add Homepage field.

  [ Stephane Glondu ]
  * New Upstream Version
  * Use debhelper 7
  * Add a build cache (for Debian debugging)
  * Set Maintainer to d-o-m, add Samuel and myself to Uploaders
  * Add DM-Upload-Allowed
  * Switch copyright to machine-parsable format
  * Add a more detailed long description for libfloat-coq

 -- Stephane Glondu <steph@glondu.net>  Wed, 11 Mar 2009 17:13:21 +0100

coq-float (1:8.1-1.0-4) unstable; urgency=low

  [ Stefano Zacchiroli ]
  * add vcs-* fields

  [ Samuel Mimram ]
  * Rebuild with latest version of coq.

 -- Samuel Mimram <smimram@debian.org>  Sat, 19 Jan 2008 16:52:13 +0100

coq-float (1:8.1-1.0-3) unstable; urgency=low

  * Rebuild with latest coq.

 -- Samuel Mimram <smimram@debian.org>  Sun, 18 Nov 2007 19:21:44 +0000

coq-float (1:8.1-1.0-2) unstable; urgency=low

  * Rebuild with latest version of coq.

 -- Samuel Mimram <smimram@debian.org>  Mon, 10 Sep 2007 00:24:19 +0200

coq-float (1:8.1-1.0-1) unstable; urgency=low

  * New upstream release.
  * Updated upstream url and watch.
  * Made the package arch all since coq libraries should be
    platform-independant.

 -- Samuel Mimram <smimram@debian.org>  Thu, 30 Aug 2007 13:08:49 +0200

coq-float (2001-1) unstable; urgency=low

  * Initial release, closes: #438613.

 -- Samuel Mimram <smimram@debian.org>  Fri, 10 Aug 2007 14:48:56 +0000
